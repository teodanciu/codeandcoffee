import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import org.scalatest.matchers.ShouldMatchers
import util.UserUtils

@RunWith(classOf[JUnitRunner])
class TestSocialUser extends FunSuite with ShouldMatchers {

  test("Full name from meetup should be split in first and rest") {
    assert(UserUtils.fullNameToFirstAndRest("John Doe")  === ("John", "Doe"))
    assert(UserUtils.fullNameToFirstAndRest("") === ("", ""))
    assert(UserUtils.fullNameToFirstAndRest("John") ===("John", ""))
    assert(UserUtils.fullNameToFirstAndRest("John Doe Don") ===("John", "Doe Don"))
  }

}
