import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import org.scalatest.matchers.ShouldMatchers
import securesocial.core.{AuthenticationMethod, IdentityId, SocialUser}
import util.UserUtils
import util.UserUtils.HeaderData

@RunWith(classOf[JUnitRunner])
class HeaderDataTest extends FunSuite with ShouldMatchers {

  val identityId = IdentityId("1234", "meetup")
  val user = SocialUser(identityId, "Eleonora", "Val", "Eleonora Val", None, Some("http://avatar.org/ele"), AuthenticationMethod("method"), None, None, None)
  val userWithoutAvatar = user.copy(avatarUrl = None)

  test("Construct HeaderData from existing user") {
    val built: HeaderData = UserUtils.buildHeaderData(Left(user))

    built.greeting.toString should be("Hi Eleonora!")
    built.avatarUrl should be("http://avatar.org/ele")
  }

  test("Construct HeaderData from existing user without avatar") {
    val built: HeaderData = UserUtils.buildHeaderData(Left(userWithoutAvatar))

    built.greeting.toString should be("Hi Eleonora!")
    built.avatarUrl should be('empty)
  }


  test("Construct HeaderData from optional, but existing user") {
    val built: HeaderData = UserUtils.buildHeaderData(Right(Some(user)))

    built.greeting.toString should be("Hi Eleonora!")
    built.avatarUrl should be("http://avatar.org/ele")
  }


  test("Construct HeaderData from optional, but existing user without avatar") {
    val built: HeaderData = UserUtils.buildHeaderData(Right(Some(userWithoutAvatar)))

    built.greeting.toString should be("Hi Eleonora!")
    built.avatarUrl should be('empty)
  }


  test("Construct HeaderData from None") {
    val built: HeaderData = UserUtils.buildHeaderData(Right(None))
    built.greeting.toString should be("Hi there!")
    built.avatarUrl should be('empty)
  }

}
