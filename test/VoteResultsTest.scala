import models.{UserMinimal, Venue, Vote}
import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner
import org.scalatest.matchers.ShouldMatchers
import service.VoteResults

@RunWith(classOf[JUnitRunner])
class VoteResultsTest extends FunSuite with ShouldMatchers {

  object voters {
    val u1 = UserMinimal("1", "f1 l1", Some("url1"))
    val u2 = UserMinimal("2", "f2 l2", None)
    val u3 = UserMinimal("3", "f3 l3", Some("url3"))
    val u4 = UserMinimal("4", "f4 l4", None)
    val u5 = UserMinimal("5", "f5 l5", Some("url5"))
    val u6 = UserMinimal("6", "f6 l6", None)
  }

  object venues {
    val v1 = Venue("v1", "p1")
    val v2 = Venue("v2", "p2")
    val v3 = Venue("v3", "p3")
    val v4 = Venue("v4", "p4")
    val v5 = Venue("v5", "p5")
    val v6 = Venue("v6", "p6")

    val allVenues = List(v1, v2, v3, v4, v5, v6)
  }

  object votes {

    import venues._
    import voters._

    val vote1 = Vote("1", List(v1, v2, v3), List(v1), Some(u1), false)
    val vote2 = Vote("2", Nil, Nil, Some(u2), false)
    val vote3 = Vote("3", List(v4, v5), List(v4, v5), Some(u3), false)
    val vote4 = Vote("4", List(v1, v2), List(v2), Some(u4), false)
    val vote5 = Vote("5", List(v5), Nil, Some(u5), false)
    val vote6 = Vote("6", List(v1, v3), List(v6), Some(u6), false)

    val allVotes = List(vote1, vote2, vote3, vote4, vote5, vote6)
  }

  import venues._
  import votes._
  import voters._

  test("Compute vote results based on given list of votes and venues") {
    val voteResults = new VoteResults(allVotes).build(allVenues)

    allVenues.foreach {
      venue =>
        voteResults should contain key venue
    }

    voteResults(v1) should be(3)
    voteResults(v2) should be(2)
    voteResults(v3) should be(2)
    voteResults(v4) should be(1)
    voteResults(v5) should be(2)
    voteResults(v6) should be(0)
  }


  test("Vote results are sorted ascending by voted") {
    val voteResults = new VoteResults(allVotes).build(allVenues).toSeq
    voteResults.head._1 should be(v6)
    voteResults(1)._1 should be(v4)
    voteResults.last._1 should be(v1)
  }


  test("Compute vote results when there are no votes") {
    val voteResults = new VoteResults(Nil).build(allVenues)
    allVenues.foreach {
      venue =>
        voteResults should contain key venue
    }

    voteResults.foreach {
      case (venue, votes) => votes should be(0)
    }
  }


  test("Compute vote results when all votes are for one venue") {
    val constantVotes = allVotes.map(_.copy(chosenVenues = List(v1)))
    val voteResults = new VoteResults(constantVotes).build(allVenues)
    allVenues.foreach {
      venue =>
        voteResults should contain key venue
    }

    voteResults(v1) should be(allVotes.size)
  }

  test("Determine voters of a venue") {
    val voteResults = new VoteResults(allVotes)

    voteResults.votersOfVenue(v1.postcode) should be(List(u1, u4, u6))
    voteResults.votersOfVenue(v2.postcode) should be(List(u1, u4))
    voteResults.votersOfVenue(v3.postcode) should be(List(u1, u6))
    voteResults.votersOfVenue(v4.postcode) should be(List(u3))
    voteResults.votersOfVenue(v5.postcode) should be(List(u3, u5))
    voteResults.votersOfVenue(v6.postcode) should be ('empty)


  }

}
