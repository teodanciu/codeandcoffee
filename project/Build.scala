import sbt._
import Keys._
import play.Project._

object ApplicationBuild extends Build {

  val appName = "codeandcoffee"
  val appVersion = "1.0-SNAPSHOT"

  val appDependencies = Seq(
    // Add your project dependencies here,
    jdbc,
    anorm,
    "securesocial" %% "securesocial" % "2.1.1",
    "org.mongodb" %% "casbah" % "2.6.2",
    "org.scalatest" %% "scalatest" % "1.9.1" % "test"
  )


  val main = play.Project(appName, appVersion, appDependencies).settings(
    resolvers += Resolver.url("sbt-plugin-releases", new URL("http://repo.scala-sbt.org/scalasbt/sbt-plugin-releases/"))(Resolver.ivyStylePatterns)
  )

}
