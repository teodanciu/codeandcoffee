package teozaurus.securesocial.providers

import play.api.{Logger, Application}
import securesocial.core._
import play.api.libs.ws.WS
import play.api.libs.oauth.RequestToken
import play.api.libs.oauth.OAuthCalculator
import util.UserUtils

class MeetupProvider(application: Application) extends OAuth1Provider(application) {
  def fillProfile(user: SocialUser): SocialUser = {

    import MeetupProvider._

    val oauthInfo = user.oAuth1Info.get

    val userServiceInfo = SecureSocial.serviceInfoFor(user)
    val key = userServiceInfo.get.key
    val requestToken = RequestToken(oauthInfo.token, oauthInfo.secret)

    val oauthCalculator = OAuthCalculator(key, requestToken)


    val call = WS.url(MemberCall).sign(oauthCalculator).get()

    try {
      val response = awaitResult(call)
      val me = response.json
      val userId = (me \ Id).as[Int]
      val name = (me \ Name).as[String]
      val profileImage = (me \ Photo \ PhotoLink).asOpt[String]

      val (firstName, lastName) = UserUtils.fullNameToFirstAndRest(name)

      user.copy(identityId = IdentityId(userId.toString, id), firstName = firstName, lastName = lastName, fullName = name, avatarUrl = profileImage)

    } catch {
      case e: Exception => {
        Logger.error("[securesocial] error retrieving profile information from Meetup", e)
        throw new AuthenticationException()
      }
    }
  }

  def id: String = MeetupProvider.Meetup
}


object MeetupProvider {
  val MemberCall = "https://api.meetup.com/2/member/self"
  val Meetup = "meetup"
  val Id = "id"
  val Name = "name"
  val Photo = "photo"
  val PhotoLink = "photo_link"
}
