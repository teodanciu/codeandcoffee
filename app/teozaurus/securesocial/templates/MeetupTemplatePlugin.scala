package teozaurus.securesocial.templates

import securesocial.controllers.TemplatesPlugin
import play.api.{Logger, Application}
import securesocial.core.{SecuredRequest, Identity}
import play.api.mvc.{Request, RequestHeader}
import play.api.templates.{Html, Txt}
import play.api.data.Form
import securesocial.controllers.PasswordChange.ChangeInfo
import securesocial.controllers.Registration.RegistrationInfo

class MeetupTemplatePlugin(application: Application) extends TemplatesPlugin {

  def getLoginPage[A](implicit request: Request[A], form: Form[(String, String)], msg: Option[String]): Html =  {
    Logger.info("Using MeetupTemplatePlugin")
    views.html.login(form, msg)
  }

  def getSignUpPage[A](implicit request: Request[A], form: Form[RegistrationInfo], token: String): Html = securesocial.views.html.Registration.signUp(form, token)


  def getStartSignUpPage[A](implicit request: Request[A], form: Form[String]): Html = securesocial.views.html.Registration.startSignUp(form)


  def getResetPasswordPage[A](implicit request: Request[A], form: Form[(String, String)], token: String): Html = securesocial.views.html.Registration.resetPasswordPage(form, token)


  def getStartResetPasswordPage[A](implicit request: Request[A], form: Form[String]): Html = securesocial.views.html.Registration.startResetPassword(form)


  def getPasswordChangePage[A](implicit request: SecuredRequest[A], form: Form[ChangeInfo]): Html = securesocial.views.html.passwordChange(form)


  def getNotAuthorizedPage[A](implicit request: Request[A]): Html = securesocial.views.html.notAuthorized()


  def getSignUpEmail(token: String)(implicit request: RequestHeader): (Option[Txt], Option[Html]) = (None, Some(securesocial.views.html.mails.signUpEmail(token)))


  def getAlreadyRegisteredEmail(user: Identity)(implicit request: RequestHeader): (Option[Txt], Option[Html]) = (None, Some(securesocial.views.html.mails.alreadyRegisteredEmail(user)))


  def getWelcomeEmail(user: Identity)(implicit request: RequestHeader): (Option[Txt], Option[Html]) = (None, Some(securesocial.views.html.mails.welcomeEmail(user)))


  def getUnknownEmailNotice()(implicit request: RequestHeader): (Option[Txt], Option[Html]) = (None, Some(securesocial.views.html.mails.unknownEmailNotice(request)))


  def getSendPasswordResetEmail(user: Identity, token: String)(implicit request: RequestHeader): (Option[Txt], Option[Html]) = (None, Some(securesocial.views.html.mails.passwordResetEmail(user, token)))


  def getPasswordChangedNoticeEmail(user: Identity)(implicit request: RequestHeader): (Option[Txt], Option[Html]) = (None, Some(securesocial.views.html.mails.passwordChangedNotice(user)))

}
