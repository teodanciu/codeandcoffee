package controllers

import play.api.mvc.{Action, Controller}
import service.{VoteResults, VenueService, VoteService}
import play.Logger
import models.{UserMinimal, Venue, Vote, JsonConverters}
import util.offline.OfflineMockAuthentication
import securesocial.core.SecureSocial
import play.api.libs.json.Json
import play.api.libs.json.Json.JsValueWrapper

object VoteController extends Controller with SecureSocial /*with OfflineMockAuthentication*/ {

  def votePage = SecuredAction {
    implicit request =>
      val maybeVote = VoteService.findUserVote(request.user)
      val chosenPostcodes = maybeVote.map(_.chosenPostcodes).getOrElse(Nil)
      val selectableVenues = VenueService.findAllSelectingPostcodes(chosenPostcodes)
      Ok(views.html.vote(selectableVenues, request.user, maybeVote.exists(_.anonymous)))
  }

  def saveVote = SecuredAction {
    implicit request =>
      request.body.asJson.fold {
        Logger.error("Could not parse " + request.body + " as json.")
      } {
        json =>
          implicit val voteReads = JsonConverters.voteReads
          Logger.info("Received vote: \n\t" + request.body + " \nfrom user: \n\t" + request.user)
          val maybeVote = json.asOpt[Vote]

          maybeVote.fold {
            Logger.warn("Could not deserialize body as Vote")
          } {
            Logger.debug("Deserialized json-vote into object: \n\t" + maybeVote)
            vote => VoteService.save(vote, request.user)
              vote.ownVenues.foreach {
                venue => VenueService.insert(venue)
              }
          }
      }
      Redirect("/venues")
  }

  def resultsPage = UserAwareAction {
    implicit request =>
      Ok(views.html.results(request.user))
  }


  /*
  [ { "venue" : "Home", "postcode": "NW19SP", "votes": 2 },
              { "venue" : "Starbucks on Oxford Street", "postcode" : "W1D2EQ", "votes" : 10 } ]
   */
  def voteResults = Action {
    val allVotes = VoteService.findAll
    val venues= VenueService.findAllWithoutDetail

    val sortedOccurrenceMap = new VoteResults(allVotes).build(venues)

    val voteToJsonMapFunction: ((Venue, Int)) => JsValueWrapper = {
      case (venue, votes) => Json.obj("venue" -> venue.name,
        "postcode" -> venue.postcode,
        "votes" -> votes)
    }

    val jsonArray = sortedOccurrenceMap.map(voteToJsonMapFunction).toSeq
    val jsonResponse = Json.arr(jsonArray: _*)

    Ok(jsonResponse)
  }


  def votersOfVenue(postcode: String) = Action {
    val allVotes = VoteService.findAll
    val voters: Seq[UserMinimal] = new VoteResults(allVotes).votersOfVenue(postcode)

    val voterToJsonMapFunction: UserMinimal => JsValueWrapper = {
      voter => Json.obj(
        "id" -> voter.id,
        "fullName" -> voter.fullName,
        "avatarUrl" -> voter.avatarUrl
      )
    }

    val jsonArray = voters.map(voterToJsonMapFunction)
    val jsonResponse = Json.arr(jsonArray: _*)
    Ok(jsonResponse)
  }

}
