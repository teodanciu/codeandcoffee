package controllers

import play.api.libs.json.Json
import play.api.mvc._
import play.api.Logger
import models._
import service.{VoteService, PostcodeService, VenueService}
import securesocial.core.SecureSocial
import util.offline.OfflineMockAuthentication
import play.api.templates.Html
import play.api.data._
import play.api.data.Forms._

object VenueController extends Controller with SecureSocial /*with OfflineMockAuthentication*/ { //TODO: remove OfflineMockAuthentication mixin, when online.

  def validateVenue(venueJsonString: String) = Action {
    implicit request =>
      Logger.info("Received venue-validation request: " + venueJsonString)
      val venueJson = Json.parse(venueJsonString)

      implicit val venueReads = JsonConverters.venueReads
      val maybeVenue = venueJson.asOpt[Venue]

      maybeVenue.fold {
        Logger.warn("Could not deserialize " + venueJsonString + " as Venue.")
        InternalServerError(Json.obj())
      } {
        Logger.debug("Deserialized json-vote into object: \n\t" + maybeVenue.get)
        venue =>
          val (venueExists, postcodeExists) = VenueService.exists(venue)
          val postcodeValid = if (postcodeExists)
            true
          else
            PostcodeService.validatePostcode(venue.postcode).getOrElse(false)

          val jsonResponse = Json.obj(
            "venue" -> Json.obj("value" -> venue.name, "isAvailable" -> !venueExists),
            "postcode" -> Json.obj("value" -> venue.postcode, "isValid" -> postcodeValid, "isAvailable" -> !postcodeExists))
          Ok(jsonResponse)
      }
  }

  def venuesPage(maybeActivePostcode: Option[String]) = UserAwareAction {
    implicit request =>
      val maybeUser = request.user
      val addedPostcodes: List[String] = maybeUser.fold(List.empty[String]) {
        user =>
          val maybeVote = VoteService.findUserVote(user)
          maybeVote.map(_.ownedPostcodes).getOrElse(Nil)
      }
      val venues: List[SelectableVenue] = VenueService.findAllSelectingPostcodes(addedPostcodes)
      val active = maybeActivePostcode.getOrElse(venues.headOption.map(_.venue.postcode).getOrElse(""))
      Ok(views.html.venues(venues, active)(maybeUser))
  }

  def editVenuePage(postcode: String) = SecuredAction {
    implicit request =>
      import VenueFormMappings.venueForm
      val maybeVenue = VenueService.findByPostcode(postcode)
      maybeVenue.fold {
        BadRequest(Html("Something went wrong. Sorry"))
      } {
        venue =>
          Ok(views.html.editVenue(venue, venueForm.fill(venue))(request.user))
      }
  }

  def saveVenue(postcode: String) = Action {
    implicit request =>
      import VenueFormMappings.venueForm
      val form = venueForm.bindFromRequest()
      form.fold(formWithErrors => routes.VenueController.editVenuePage(postcode),
        venue => {
          VenueService.update(venue)
          Redirect(routes.VenueController.venuesPage(Some(postcode)))
        }
      )
      Redirect(routes.VenueController.venuesPage(Some(postcode)))
  }


  object VenueFormMappings {
    val openingMapping = mapping(
      "from" -> number,
      "to" -> number
    )(OpeningHours.apply)(OpeningHours.unapply)

    val detailsMapping = mapping(
      "address" -> optional(text),
      "openWithin" -> optional(openingMapping),
      "contact" -> optional(text),
      "notes" -> optional(text)
    )(VenueDetails.apply)(VenueDetails.unapply)

    val venueForm = Form(
      mapping(
        "name" -> text,
        "postcode" -> text,
        "details" -> optional(detailsMapping)
      )(Venue.apply)(Venue.unapply)
    )

  }
}
