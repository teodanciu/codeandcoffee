package controllers

import play.api.mvc._
import securesocial.core.{Identity, Authorization, SecureSocial}

object Application extends Controller with SecureSocial  {

  def index = Action {
    Ok(views.html.index("hello"))
  }

  def aboutPage = Action {
    Ok(views.html.about())
  }

  def javascriptRoutes = Action {
    implicit request =>
      import play.api.Routes
      Ok(
        Routes.javascriptRouter("jsRoutes")(routes.javascript.VoteController.saveVote,
          routes.javascript.VenueController.validateVenue,
          routes.javascript.VoteController.voteResults,
          routes.javascript.VoteController.votersOfVenue)).as("text/javascript")
  }


  // a sample action using the new authorization hook
  def onlyTwitter = SecuredAction(WithProvider("twitter")) {
    implicit request =>
    //
    //    Note: If you had a User class and returned an instance of it from UserService, this
    //          is how you would convert Identity to your own class:
    //
    //    request.user match {
    //      case user: User => // do whatever you need with your user class
    //      case _ => // did not get a User instance, should not happen,log error/thow exception
    //    }
      Ok("You can see this because you logged in using Twitter")
  }


  def break[T](param: T) {
    println("breaking...param: " + param)
  }

  def loggedIn = SecuredAction {
    implicit request =>
      Ok(views.html.loggedin(request.user))
  }
}

// An Authorization implementation that only authorizes uses that logged in using twitter
case class WithProvider(provider: String) extends Authorization {
  def isAuthorized(user: Identity) = {
    user.identityId.providerId == provider
  }
}
