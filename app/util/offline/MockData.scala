package util.offline

import securesocial.core.{IdentityId, Identity, UserService}

object MockData {

  val User:Identity = UserService.find(IdentityId("57770372", "meetup")).get

}
