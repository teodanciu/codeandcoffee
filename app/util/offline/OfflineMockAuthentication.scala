package util.offline

import play.api.mvc._
import securesocial.core.{Identity, RequestWithUser}


case class MockUserRequest[A](user: Identity, request: Request[A]) extends WrappedRequest(request)

trait OfflineMockAuthentication extends Controller {
  def SecuredAction[A](p: BodyParser[A])
                                 (f: MockUserRequest[A] => Result): Action[A] =
    Action(p) {
      implicit request => {
        val mockUser = MockData.User
        f(MockUserRequest(mockUser, request))
      }
    }

  def SecuredAction(f: MockUserRequest[AnyContent] => Result): Action[AnyContent] =
    SecuredAction(p = parse.anyContent)(f)


  def UserAwareAction[A](p: BodyParser[A])(f: RequestWithUser[A] => Result) = Action(p) {
    implicit request => {
      val user = Some(MockData.User)
      f(RequestWithUser(user, request))
    }
  }

  def UserAwareAction(f: RequestWithUser[AnyContent] => Result): Action[AnyContent] = {
    UserAwareAction(parse.anyContent)(f)
  }

}
