package util

import securesocial.core.Identity
import play.api.templates.Html

object UserUtils {
  def fullNameToFirstAndRest(name: String): (String, String) =
    name.split("\\s+").toList match {
      case f :: xs => (f, xs.mkString(" "))
      case _ => ("", "")
    }


  def buildHeaderData(userOrMaybeUser: Either[Identity, Option[Identity]]): HeaderData = {
    val prefix = "Hi"
    val anonymousPrefix = "Hi there"
    val suffix = "!"

    lazy val (greeting, avatarUrl): (String, String) = userOrMaybeUser.fold(
      user => headerDataForUser(user),
      maybeUser => maybeUser.map(headerDataForUser).getOrElse((anonymousPrefix + suffix, ""))
    )

    def headerDataForUser(user: Identity): (String, String) =
      (prefixedFirstName(user) + suffix, getOrElseEmptyString(user.avatarUrl))

    def prefixedFirstName(user: Identity): String =
      prefix + " " + user.firstName

    def getOrElseEmptyString(maybe: Option[String]): String =
      maybe.getOrElse("")

    HeaderData(Html(greeting), avatarUrl)
  }

  case class HeaderData(greeting: Html, avatarUrl: String)

}
