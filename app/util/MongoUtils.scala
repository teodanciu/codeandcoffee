package util

import com.mongodb.casbah.Imports._
import com.mongodb.casbah.commons.MongoDBObject

object MongoUtils {
  def mongoListFromObject(name: String)(implicit mongoObject: DBObject): MongoDBList = mongoObject.as[MongoDBList](name)

  def mongoObject[A <: String, B](elems: (A, B)*): DBObject = {
    val definedElems = elems.flatMap {
      case (a, b: Option[_]) if !b.isDefined => None
      case (a, b: Option[_]) if b.isDefined => Some(a -> b.get)
      case (a, b) => Some(a -> b)
    }
    MongoDBObject(definedElems: _*)
  }

  def maybeConvertedMongoObject[T, U](obj: T)(converter: U => DBObject): Option[DBObject] = {
    obj match {
      case opt: Option[U] => opt.map(converter)
      case u: U => Some(converter(u))
    }
  }
}

