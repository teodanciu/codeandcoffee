package models

import securesocial.core._
import com.mongodb.casbah.Imports._
import com.mongodb.casbah.commons.MongoDBObject
import scala.Some
import securesocial.core.OAuth1Info
import util.MongoUtils.mongoObject

object User {
  def asSearchObject(user: Identity): DBObject = {
    MongoDBObject("_id" -> user.identityId.userId)
  }

  def toMongo(user: Identity): MongoDBObject = {
    MongoDBObject(
      "_id" -> user.identityId.userId,
      "firstName" -> user.firstName,
      "lastName" -> user.lastName,
      "fullName" -> user.fullName,
      "email" -> user.email.getOrElse(""),
      "avatarUrl" -> user.avatarUrl.getOrElse("#"),
      "auth" -> MongoDBObject(
        "authMethod" -> user.authMethod.method,
        "oauth1Token" -> user.oAuth1Info.map(_.token).getOrElse(""),
        "oauth1Secret" -> user.oAuth1Info.map(_.secret).getOrElse("")
      )
    )
  }

  def fromMongo(mongoObject: DBObject): Identity = {
    object Auth {
      val auth = mongoObject.as[DBObject]("auth")
      val authMethod = auth("authMethod").toString

      val oauth1Token = auth("oauth1Token").toString
      val oauth1Secret = auth("oauth1Secret").toString
    }

    SocialUser(
      identityId = IdentityId(mongoObject("_id").toString, "meetup"),
      firstName = mongoObject("firstName").toString,
      lastName = mongoObject("lastName").toString,
      fullName = mongoObject("fullName").toString,
      email = Some(mongoObject("email").toString),
      avatarUrl = Some(mongoObject("avatarUrl").toString),
      authMethod = AuthenticationMethod(Auth.authMethod),
      oAuth1Info = Some(OAuth1Info(Auth.oauth1Token, Auth.oauth1Secret))
    )
  }

}


case class UserMinimal(id: String, fullName: String, avatarUrl: Option[String])

object UserMinimal {
  def toMongo(user: UserMinimal): MongoDBObject = {
    mongoObject("id" -> user.id,
      "fullName" -> user.fullName,
      "avatarUrl" -> user.avatarUrl.getOrElse(""))
  }

  def fromMongo(mongoObject: DBObject): UserMinimal =
    UserMinimal(id = mongoObject("id").toString,
      fullName = mongoObject("fullName").toString,
      avatarUrl = Some(mongoObject("avatarUrl").toString))
}
