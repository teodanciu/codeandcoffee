package models

import com.mongodb.casbah.Imports._
import util.MongoUtils.{mongoObject, maybeConvertedMongoObject}

case class VenueDetails(address: Option[String], openWithin: Option[OpeningHours], contact: Option[String], notes: Option[String])

object VenueDetails {

  def toMongo(details: VenueDetails): DBObject =
    mongoObject(
      "address" -> details.address,
      "openWithin" -> maybeConvertedMongoObject(details.openWithin)(OpeningHours.toMongo),
      "contact" -> details.contact,
      "notes" -> details.notes
    )

  def fromMongo(mongoObject: DBObject): VenueDetails =
    VenueDetails(address = mongoObject.getAs[String]("address"),
      openWithin = mongoObject.getAs[DBObject]("openWithin").map(OpeningHours.fromMongo),
      contact = mongoObject.getAs[String]("contact"),
      notes = mongoObject.getAs[String]("notes"))
}

case class OpeningHours(from: Int, to: Int) {
  require(between0And24(from))
  require(between0And24(to))

  def between0And24: Int => Boolean = number => number >= 0 && number <= 24
}


object OpeningHours {
  def toMongo(openingHours: OpeningHours): DBObject =
    mongoObject(
      "from" -> openingHours.from,
      "to" -> openingHours.to
    )

  def fromMongo(mongoObject: DBObject): OpeningHours =
    OpeningHours(mongoObject.as[Int]("from"), mongoObject.as[Int]("to"))
}

