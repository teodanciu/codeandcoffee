package models

import play.api.libs.json.{JsPath, Reads}

object JsonConverters {

  import play.api.libs.json.Reads._
  import play.api.libs.functional.syntax._

  implicit val openingHoursReads: Reads[OpeningHours] = (
    (JsPath \ "from").read[Int] and
      (JsPath \ "to").read[Int]
    )((from, to) => OpeningHours(from, to))

  implicit val venueDetailsReads: Reads[VenueDetails] = (
    (JsPath \ "address").readNullable[String] and
      (JsPath \ "openWithin").readNullable[OpeningHours] and
      (JsPath \ "contact").readNullable[String] and
      (JsPath \ "notes").readNullable[String]
    )((address, openWithin, contact, notes) => VenueDetails(address, openWithin, contact, notes))

  val venueReads: Reads[Venue] = (
    (JsPath \ "venue").read[String] and
      (JsPath \ "postcode").read[String] and
      (JsPath \ "details").readNullable[VenueDetails]
    )(Venue.apply _)

  val oldVenuesReads: Reads[List[Venue]] = (JsPath \ "oldVenues").read(Reads.list[Venue](venueReads))
  val newVenuesReads: Reads[List[Venue]] = (JsPath \ "newVenues").read(Reads.list[Venue](venueReads))
  val idReads: Reads[String] = (JsPath \ "userId").read[String]
  val anonymousReads: Reads[Boolean] = (JsPath \ "anonymous").read[Boolean]

  //we are not reading user details from js - we already have them on server.
  val voteReads: Reads[Vote] = (idReads and oldVenuesReads and newVenuesReads and anonymousReads)(
    (id, oldVenues, newVenues, anonymous) =>
      Vote(userId = id,
        chosenVenues = oldVenues ++ newVenues,
        ownVenues = newVenues,
        voter = None,
        anonymous = anonymous)
  )
}
