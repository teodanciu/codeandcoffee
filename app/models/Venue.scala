package models

import com.mongodb.casbah.Imports._
import util.MongoUtils.{mongoObject, maybeConvertedMongoObject}

case class Venue(name: String, postcode: String, venueDetails: Option[VenueDetails] = None)

object Venue {

  def toMongo(venue: Venue): DBObject =
    mongoObject("venue" -> venue.name,
      "postcode" -> venue.postcode,
      "details" -> maybeConvertedMongoObject(venue.venueDetails)(VenueDetails.toMongo))

  def fromMongo(mongoObject: DBObject): Venue = {
    val venueDetails = mongoObject.getAs[DBObject]("details").map(VenueDetails.fromMongo)
    fromMongoWithoutDetail(mongoObject).copy(venueDetails = venueDetails)
  }

  def fromMongoWithoutDetail(mongoObject: DBObject): Venue =
    Venue(name = mongoObject.as[String]("venue"),
      postcode = mongoObject.as[String]("postcode"))

  def venuesFromMongoList(venues: MongoDBList): List[Venue] =
    (for {
      (_, i) <- venues.view.zipWithIndex
      venue = venues.as[DBObject](i)
      name = venue.as[String]("venue")
      postcode = venue.as[String]("postcode")
    } yield Venue(name, postcode)).toList

  def asSearchObject(postcode: String): DBObject =
    mongoObject("postcode" -> postcode)

}

case class SelectableVenue(venue: Venue, selected: Boolean)

object SelectableVenue {
  val venueToSelectableVenue: (List[String] => Venue => SelectableVenue) =
    postcodes =>
      venue =>
        SelectableVenue(venue, postcodes.contains(venue.postcode))
}