package models

import com.mongodb.casbah.commons.MongoDBList
import com.mongodb.casbah.Imports._
import util.MongoUtils.{mongoListFromObject, mongoObject, maybeConvertedMongoObject}
import securesocial.core.Identity

case class Vote(userId: String, chosenVenues: List[Venue], ownVenues: List[Venue], voter: Option[UserMinimal], anonymous: Boolean) {
  def copiedWithVoter(user: Identity): Vote = {
    val userMinimal = UserMinimal(id = user.identityId.userId, fullName = user.fullName, avatarUrl = user.avatarUrl)
    this.copy(voter = Some(userMinimal))
  }

  def chosenPostcodes: List[String] = postcodes(chosenVenues)
  def ownedPostcodes: List[String] = postcodes(ownVenues)

  def voterExposed = if (anonymous) Some(VoterAnonymous) else voter

  private def postcodes(venues: List[Venue]): List[String] =
    venues.map(_.postcode)

}

object VoterAnonymous extends UserMinimal("", "anonymous", Some("/assets/images/anonymousAvatar.png"))


object Vote {

  def toMongo(vote: Vote): DBObject = {
    val venueToMongo: Venue => DBObject =
      venue =>
        mongoObject("venue" -> venue.name, "postcode" -> venue.postcode)

    def venuesToMongoList(venues: List[Venue]): MongoDBList = {
      val builder = MongoDBList.newBuilder
      venues.map(venue => builder.+=(venueToMongo(venue)))

      builder.result()
    }

    mongoObject("$set" -> mongoObject(
      "chosenVenues" -> venuesToMongoList(vote.chosenVenues),
      "voter" -> maybeConvertedMongoObject(vote.voter)(UserMinimal.toMongo),
      "anonymous" -> vote.anonymous),
      "$pushAll" -> mongoObject("ownVenues" -> venuesToMongoList(vote.ownVenues)))
  }

  def fromMongo(mongoObject: DBObject): Vote = {
    val userId = mongoObject("_id").toString
    val oldVenues = Venue.venuesFromMongoList(mongoListFromObject("chosenVenues")(mongoObject))
    val newVenues = Venue.venuesFromMongoList(mongoListFromObject("ownVenues")(mongoObject))
    val user = mongoObject.getAs[DBObject]("voter").map(UserMinimal.fromMongo)
    val anonymous = mongoObject.as[Boolean]("anonymous")
    Vote(userId,
      chosenVenues = oldVenues,
      ownVenues = newVenues,
      voter = user,
      anonymous = anonymous)
  }

  def asSearchObject(vote: Vote): DBObject = {
    mongoObject("_id" -> vote.userId)
  }
}