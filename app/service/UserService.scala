package service

import play.api.{Logger, Application}
import securesocial.core._

import com.mongodb.casbah.Imports._
import securesocial.core.providers.Token
import com.mongodb.MongoException
import models.User

class UserService(application: Application) extends UserServicePlugin(application) {

  lazy val userCollection = MongoAccess.userCollection

  def find(id: IdentityId): Option[Identity] = {
    Logger.debug("Looking up user with id: " + id)
    userCollection.findOneByID(id.userId).map(User.fromMongo)
  }

  def save(user: Identity): Identity = {
    Logger.debug("Saving user: " + user)
    try {
      userCollection.insert(User.toMongo(user))
    } catch {
      case e: MongoException.DuplicateKey => {
        Logger.info("User found, not saving.")
      }
    }
    user
  }

  def findByEmailAndProvider(email: String, providerId: String): Option[Identity] =
    throw new NotImplementedError("UsernamePassword provider not used: no accounts created other than meetup ones.")

  def save(token: Token) =
    throw new NotImplementedError("UsernamePassword provider not used: no accounts created other than meetup ones.")

  def findToken(token: String): Option[Token] =
    throw new NotImplementedError("UsernamePassword provider not used: no accounts created other than meetup ones.")

  def deleteToken(uuid: String) =
    throw new NotImplementedError("UsernamePassword provider not used: no accounts created other than meetup ones.")

  def deleteExpiredTokens() =
    throw new NotImplementedError("UsernamePassword provider not used: no accounts created other than meetup ones.")
}
