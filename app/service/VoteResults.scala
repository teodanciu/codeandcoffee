package service

import models.{UserMinimal, Venue, Vote}
import scala.collection.immutable.ListMap

class VoteResults(votes: List[Vote]) {
  private lazy val allChosenVenues = votes.flatMap(_.chosenVenues)

  def build(venues: List[Venue]): Map[Venue, Int] = {
    val votedOccurrenceMap: Map[Venue, Int] = venuesWithOccurrenceNumber
    val venuesNotVoted = venues.filterNot(venue => allChosenVenues.contains(venue))

    val allVenuesOccurrenceMap = complementOccurrenceMapWithVenues(votedOccurrenceMap, venuesNotVoted)
    ListMap(sortOccurrenceMap(allVenuesOccurrenceMap): _*)
  }


  def votersOfVenue(postcode: String): Seq[UserMinimal] = {
    (for {
      vote <- votes
      venue <- vote.chosenVenues if venue.postcode == postcode
    }  yield vote.voterExposed).flatten
  }


  private def venuesWithOccurrenceNumber: Map[Venue, Int] =
    allChosenVenues.groupBy(identity).mapValues(_.size)

  private def complementOccurrenceMapWithVenues(occurrenceMap: Map[Venue, Int], extraVenues: List[Venue]): Map[Venue, Int] = {
    val venuesNotVotedMap = extraVenues.map(_ -> 0)
    occurrenceMap ++ venuesNotVotedMap
  }

  private def sortOccurrenceMap(occurrenceMap: Map[Venue, Int]): Seq[(Venue, Int)] =
    occurrenceMap.toSeq.sortWith(_._2 < _._2)


}
