package service

import models.{Venue, User, Vote}
import play.api.Logger
import securesocial.core.Identity

object VoteService {

  lazy val voteCollection = MongoAccess.voteCollection

  def save(vote: Vote, user: Identity) = {
    import Vote._
    val voteWithVoter = vote.copiedWithVoter(user)
    val mongoVote = toMongo(voteWithVoter)
    Logger.info("Saving: " + vote + " \n \t represented as: " + mongoVote)
    voteCollection.update(asSearchObject(vote), mongoVote, upsert = true)
  }

  def findUserVote(user: Identity): Option[Vote] =
    voteCollection.findOne(User.asSearchObject(user)).map(Vote.fromMongo)

  def findAll: List[Vote] = {
    voteCollection.find().map(Vote.fromMongo).toList
  }
}
