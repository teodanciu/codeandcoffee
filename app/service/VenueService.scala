package service

import models.{Vote, SelectableVenue, Venue}
import com.mongodb.casbah.commons.MongoDBList
import play.api.Logger

import util.MongoUtils.mongoObject

object VenueService {

  lazy val venueCollection = MongoAccess.venueCollection

  def insert(venue: Venue) = {
    val mongoVenue = Venue.toMongo(venue)
    venueCollection.insert(mongoVenue)
  }

  def update(venue: Venue) = {
    val searchObject = Venue.asSearchObject(venue.postcode)
    venueCollection.update(searchObject, Venue.toMongo(venue), upsert = false)
  }

  def findAll: List[Venue] = venueCollection.find()
    .map(Venue.fromMongo).toList
    .sortWith(_.name < _.name)

  def findAllWithoutDetail: List[Venue] = venueCollection.find()
    .map(Venue.fromMongoWithoutDetail).toList
    .sortWith(_.name < _.name)

  def findAllSelectingPostcodes(postcodes: List[String]): List[SelectableVenue] = {
    VenueService.findAll.map {
      SelectableVenue.venueToSelectableVenue(postcodes)
    }
  }

  def findByPostcode(postcode: String): Option[Venue] =
    venueCollection.findOne(Venue.asSearchObject(postcode)).map(Venue.fromMongo)


  def exists(venue: Venue): (Boolean, Boolean) = {
    val nameJson = mongoObject("venue" -> venue.name)
    val postCodeJson = mongoObject("postcode" -> venue.postcode)
    val list = MongoDBList.newBuilder.+=(nameJson).+=(postCodeJson).result()
    val queryObject = mongoObject("$or" -> list)
    val retrievedObjects = venueCollection.find(queryObject).toList
    Logger.debug(s"Executed mongo query $queryObject with result: $retrievedObjects")
    val retrievedVenues = retrievedObjects.map(Venue.fromMongo)
    Pair(
      retrievedVenues.find(_.name == venue.name).fold(false)(_ => true),
      retrievedVenues.find(_.postcode == venue.postcode).fold(false)(_ => true)
    )
  }

  def mockVenues: List[Venue] = List(Venue("Pret in Kings' Cross", "N19XW"),
    Venue("Starbucks in Camden", "NW19SP"),
    Venue("Nera on Oxford Street", "WC12AB"))
}
