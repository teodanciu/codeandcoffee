package service

import play.api.libs.ws.{Response, WS}
import scala.concurrent.{ExecutionContext, Future, Await}

import concurrent.duration._
import play.api.http.Status
import play.api.Logger

object PostcodeService {


  def validationUrl(implicit postcode: String) = s"http://www.uk-postcodes.com/postcode/$postcode.json"

  def validationUrlAlternative(implicit postcode: String) = s"http://mapit.mysociety.org/postcode/$postcode"


  import ExecutionContext.Implicits.global

  def validatePostcode(postcode: String): Option[Boolean] = {
    val url = validationUrl(postcode)
    val urlAlt = validationUrlAlternative(postcode)

    tryWSOrElse(url,
      tryWSOrElse(urlAlt, None))
      .map(_.status == Status.OK)
  }

  def tryWSOrElse(url: String, recoverStrategy: => Option[Response]): Option[Response] = {
    val future: Future[Option[Response]] = WS.url(url).get().map(response => Some(response)).recover {
      case e =>
        Logger.error(s"Got exception when trying to validate postcode at url $url", e)
        recoverStrategy

    }
    val maybeResponse = Await.result(future, 10.seconds)
    maybeResponse
  }


}
