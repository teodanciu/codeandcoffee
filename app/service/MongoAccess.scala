package service

import com.mongodb.casbah.MongoURI
import play.api.Play.current
import play.api.{Play, Logger}
import com.mongodb.casbah.Imports._

object MongoAccess {

  import MongoConfig._

  Logger.info("Trying to connect to mongo on: " + uri.hosts.headOption)
  private val db = MongoConnection(uri)(databaseName)
  authenticate()

  val userCollection = db("users")
  val voteCollection = db("votes")
  val venueCollection = db("venues")

  def authenticate() = {
    if (!db.isAuthenticated)
      db.authenticate(username, uri.password.foldLeft("")(_ + _.toString))
    Logger.info("Connection to mongo successful.")
  }

}


object MongoConfig {
  private val configuredMongoUri = Play.configuration.getString("mongo.uri").getOrElse(throwMongoException("Could not retrieve mongo.uri configuration"))
  val uri = MongoURI(configuredMongoUri)

  val databaseName = uri.database.getOrElse(throwMongoException("Could not connect using mongo.uri" + uri))
  val username = uri.username.getOrElse("")

  def throwMongoException(message: String) = throw new IllegalArgumentException(message)
}