function createMap(divid, postcode) {
    var geocoder = new google.maps.Geocoder();

    geocoder.geocode({ 'address': postcode }, function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var mapOptions = {
                zoom: 15,
                center: results[0].geometry.location,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById(divid), mapOptions);
            new google.maps.Marker({
                map: map,
                position: mapOptions.center
            })
        } else {
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });
}