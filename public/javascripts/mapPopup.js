var getMap = function (opts) {
    var src = "http://maps.googleapis.com/maps/api/staticmap?",
        params = $.extend({
            zoom: 14,
            size: '400x200',
            maptype: 'roadmap',
            sensor: false
        }, opts),
        query = [];

    $.each(params, function (k, v) {
        query.push(k + '=' + encodeURIComponent(v));
    });

    src += query.join('&');

    //caching in browser
    image = new Image(400, 200);
    image.src = src;

    return '<img src="' + src + '" />';
}

function generatePopupContent(name, postcode, href) {
    var detailsLink = '<a href=' + href + '>Details</a>';

    var postcodeSpan = "<span class='marginright10'>" + postcode + "</span>";
    var content = "";
    content += "<span class='float-right'>" + postcodeSpan + detailsLink + "</span>";
    content += "<div>" + name + "</div>"

    $("#popup-" + postcode).attr("data-content", content);

}


function generatePopupTitle(postcode) {
    var content = getMap({center: postcode, markers: postcode});
    $("#popup-" + postcode).attr("data-original-title", content); //    return postcode;

}