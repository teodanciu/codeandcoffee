function initializeChart() {
    jsRoutes.controllers.VoteController.voteResults().ajax({
        contentType: 'application/json',
        success: function (reply) {
            var chartData = jsonArrayToChartArray(reply);
            drawChart(chartData, 50 + 50 * chartData.length)
        },
        error: function (reply) {
            window.alert("An error has occurred. Sorry.");
        }
    });
}


function jsonArrayToChartArray(jsonArray) {
    var linkPrefix = "/venues?active="
    var data = []
    $.each(jsonArray, function (i, item) {
        var linkToVenue = linkPrefix + item.postcode
        var label = "<a href='" + linkToVenue + "'>" + item.venue + "</a>";
        data.push([item.votes, label, item.postcode, item.venue])
    });
    return data;
}

function drawChart(data, height, width) {
    $.jqplot('chart', [data], {
        seriesColors: [ "#4bb2c5", "#579575", "#839557", "#958c12", "#eaa228", "#c5b47f", "#cd7058", "#9e66ab", "#4b5de4", "#d8b83f" ],
        stackSeries: false,
        height: height,
        title: 'Votes per Venue',
        animate: !$.jqplot.use_excanvas,
        seriesDefaults: {
            renderer: $.jqplot.BarRenderer,
            shadowAngle: 135,
            rendererOptions: {
                barDirection: 'horizontal',
                highlightMouseDown: true,
                varyBarColor: true,
                barWidth: 25,
                animation: {
                    speed: 200
                }
            }
        },
        axes: {
            yaxis: {
                renderer: $.jqplot.CategoryAxisRenderer,
                tickRenderer: $.jqplot.AxisTickRenderer,
                tickOptions: {
                    mark: null,
                    showGridline: false
                }
            },
            xaxis: {
                renderer: $.jqplot.LinearAxisRenderer,
                tickOptions: {
                    formatString: '%d'
                },
                min: 0
            }
        },
        highlighter: {
            show: true,
            sizeAdjust: 20,
            tooltipLocation: 'nw',
            tooltipOffset: 0,
            showMarker: false,
            tooltipAxes: 'x',
            tooltipContentEditor: function (str, seriesIndex, pointIndex, plot) {
                var date = plot.data[seriesIndex][pointIndex][0]
                var html = "<div style='padding:5x 5px'>";
                html += date;
                html += "  votes</div>";

                return html;
            }
        },
        cursor: {
            show: false
        }
    });

    $('#chart').bind('jqplotDataClick',
        function (ev, seriesIndex, pointIndex, data) {
            getVotersFromServerAndDisplay(data[2], data[3])
        }
    );

    $('.jqplot-yaxis-tick')
        .css({ cursor: "pointer", zIndex: "1" })
}

function getVotersFromServerAndDisplay(postcode, venue) {
    jsRoutes.controllers.VoteController.votersOfVenue(postcode).ajax({
        contentType: 'application/json',
        success: function (reply) {
            showVoters(reply, venue);
        },
        error: function (reply) {
            window.alert("An error has occurred. Sorry.");
        }
    });
}

function showVoters(jsonArray, venue) {
    var message = "";
    $.each(jsonArray, function (i, item) {
        var id = item.id;
        var name = item.fullName;
        var avatarUrl = item.avatarUrl;
        var meetupUrl = buildMeetupUrlIfPossible(id, name);
        message += "<br>" + "<img width='40px' height='40px' src='" + avatarUrl + "'> &nbsp;&nbsp;" + meetupUrl + "<br>";
    });

    bootbox.dialog(message, [
        {
            "label": "OK",
            "class": "btn-primary"
        }
    ], {"header": "@" + venue});
}


function buildMeetupUrlIfPossible(id, name) {
    if (id) {
        return "<a href='http://www.meetup.com/members/" + id + "'>" + name +"</a>";
    }  else return name
}