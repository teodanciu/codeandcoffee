function validateNewVenueFormAndSendIfValid() {
    var isValidForm = $('#newVenueForm').valid();
    if (isValidForm) {
        var isValidOnClient = validateVenueOnClient();
        if (isValidOnClient) {
            validateVenueOnServerAndSaveIfValid();
        }
    }
}

function validateVenueOnClient() {
    var newVenue = $("#newVenue").val();
    var newPostcode = $("#newPostcode").val();

    var newVenues = collectNewVenues();
    var isValid = true;
    var message = "";

    for (var v in newVenues) {
        var postcode = newVenues[v].postcode;
        var venue = newVenues[v].venue;

        if (newVenue == venue) {
            message += "<br> You have already added venue <b> " + venue + "</b>.";
            isValid = false;
        }

        if (newPostcode == postcode) {
            message += "<br> You have already added postcode <b>" + postcode + " </b>. ";
            isValid = false;
        }

        if (!isValid) {
            bootbox.alert("Can't add this venue, because: <br>" + message)
        }
    }
    return isValid;
}

function validateVenueOnServerAndSaveIfValid() {
    var newVenue = $("#newVenue").val();
    var newPostcode = $("#newPostcode").val();

    var json = {venue: newVenue, postcode: newPostcode};
    var stringifiedJson = JSON.stringify(json);

    jsRoutes.controllers.VenueController.validateVenue(stringifiedJson).ajax({
        data: stringifiedJson,
        dataType: 'json',
        contentType: 'application/json',
        success: function (reply) {
            handleVenueValidationResponse(reply);
        },
        error: function (reply) {
            bootbox.alert("An error has occurred. Sorry.");
        }
    });
}

function handleVenueValidationResponse(validationResponseJson) {
//   json format:  {venue: {value: "Pret in Camden", isAvailable:true}, postcode: {value: "N19XW", isValid:true, isAvailable:false}}

    var venue = validationResponseJson.venue.value;
    var venueAvailable = validationResponseJson.venue.isAvailable;
    var postcode = validationResponseJson.postcode.value;
    var postcodeValid = validationResponseJson.postcode.isValid;
    var postcodeAvailable = validationResponseJson.postcode.isAvailable;

    var hasError = false;
    var message = "";
    if (!venueAvailable) {
        message += "<br> Venue  <b> " + venue + "</b> is already registered.";
        hasError = true;
    }
    if (!postcodeValid) {
        message += "<br> <b>" +  postcode + "</b> is not a valid postcode. "
        hasError = true;
    }
    if (!postcodeAvailable) {
        message += "<br> <b>" + postcode + " </b> is already registered. "
        hasError = true;
    }

    if (hasError) {
        bootbox.alert("Can't add this venue, because: <br>" + message)
    } else {
        addVenue();
    }
}
1

function addVenue() {
    var newVenuesContainer = $("#newVenues");
    var newVenueInput = $("#newVenue");
    var newPostcodeInput = $("#newPostcode");

    var newVenue = newVenueInput.val();
    var newPostcode = newPostcodeInput.val();
    var newCheckboxId = newPostcode + _.uniqueId();
    var newCheckboxIdSelector = "#" + newCheckboxId;

    var code = '<div class="control-group colored border-danger">\
                                            <a id="" class="control-label details pop"></a>\
                                            <label class="control-label switchcontrol" for="">PLACEHOLDER_NEW_VENUE</label> \
                                            <input type="hidden" value="PLACEHOLDER_NEW_POSTCODE"> \
                                            <div class="controls switchcontrol">  \
                                                <div id="PLACEHOLDER_NEW_ID" class="switch" data-on-label="<i class=\'icon-ok icon-white\'></i>" data-off-label="<i class=\'icon-remove\'></i>">\
                                                    <input id="" type="checkbox" checked> \
                                                </div> \
                                            </div> \
                                        </div>';
    var interpolatedHtml = code.replace("PLACEHOLDER_NEW_ID", newCheckboxId).replace("PLACEHOLDER_NEW_VENUE", newVenue).replace("PLACEHOLDER_NEW_POSTCODE", newPostcode);
    newVenuesContainer.append(interpolatedHtml);

    newVenueInput.val('');
    newPostcodeInput.val('');

    var newCheckbox = $(newCheckboxIdSelector);
    var controlGroup = newCheckbox.parent().parent();

    newCheckbox.on('switch-change', function (e, data) {
        controlGroup.remove();
    });

    newCheckbox.bootstrapSwitch();
}