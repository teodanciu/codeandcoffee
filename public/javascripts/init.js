function initializeSwitch() {
    var checkedInputs = $('.switch input:checked')
    checkedInputs.each(function () {
        $(this).parents().eq(3).addClass("colored")
    });

    $('.switch').on('switch-change', function (e, data) {
        $(this).parent().parent().toggleClass("colored");
    })
}

function initializeMapPopup() {
    $('.pop').popover({ html: true})

    $(':not(#anything)').on('click', function (e) {
        $('.pop').each(function () {
            //the 'is' for buttons that trigger popups
            //the 'has' for icons and other elements within a button that triggers a popup
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
                return;
            }
        });
    });
}

function initializeNewVenueValidator() {
    jQuery.validator.addMethod("nowhitespace", function (value, element) {
        return this.optional(element) || /^\S+$/i.test(value);
    }, "No white space please");

    $("#newVenueForm").validate({
        rules: {
            newVenue: "required",
            newPostcode: {
                required: true,
                nowhitespace: true
            }
        },

        messages: {
            newVenue: "Venue name, please.",
            newPostcode: "Postcode without white spaces, please."
        }
    })
}

function selectIfNecessary(id, selected) {
    if (selected == "true") {
        var checkbox = $("#" + id)
        checkbox.prop('checked', true);
    }
}