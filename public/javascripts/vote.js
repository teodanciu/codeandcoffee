function collectNewVenues() {
    var json = $("#newVenues").find(".control-group").map(function () {
        var newVenue = $(this).find("> label").first().text();
        var newPostcode = $(this).find("> input:hidden").first().attr("value");
        return {
            venue: newVenue,
            postcode: newPostcode
        };
    }).get();
    return json;
}

function collectCheckedOldVenues() {
    var json = $("#oldVenues").find("> div.control-group > div.controls > div.switch input:checked").map(function () {
        return {
            venue: $(this).attr("name"),
            postcode: $(this).attr("id")
        }
    }).get();
    return json;
}

function collectChoice() {
    var oldVenues = collectCheckedOldVenues();
    var newVenues = collectNewVenues();
    var anonymous = $("#anonymous").is(':checked')
    var json = {
        userId: userId,
        oldVenues: oldVenues,
        newVenues: newVenues,
        anonymous: anonymous
    };
    return json;
}

function confirmAndSendVoteToServer(json) {
   var warning = "";
   if (json.newVenues.length > 0) {
       warning = "<br> <br> Note: you're adding new venue(s)."
   }
    bootbox.confirm("Really?" + warning, "No", "Yes!", function (result) {
        if (result) {
            sendVoteToServer(json)
        }
    });
}

function sendVoteToServer(json) {
    jsRoutes.controllers.VoteController.saveVote().ajax({
        data: JSON.stringify(json),
        contentType: 'application/json',
        error: function (reply) {
            bootbox.alert("An error has occurred. Sorry.");
        },
        success: function(reply) {
            bootbox.alert("Okay, got it. Thanks! &nbsp;&nbsp;&nbsp; <a href='/'>  See results</a>");
        }

    });
}
